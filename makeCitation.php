<!DOCTYPE html>
<html>
	<head>
		<link type="text/css" rel="stylesheet" href="stylesheet.css"/>
		<title>Parking Violations</title>
	</head>
	<body>
	<div id="header">
		<h1 style="text-align:left">Make Citation</h1>
	</div>
	
	<?php
		require_once('query.php');
		makeUserHeader();
	?>
	
	<p/>
	
	<div id="section">
	<form action="makeCit.php" method="post">
	<fieldset>
		citnum:&nbsp;&nbsp;<br/>
		<input type="text" name="citnum"/>
		<br/>
		description:&nbsp;&nbsp;<br/>
		<input type="text" name="description"/>
		<br/>
		cdate:&nbsp;&nbsp;<br/>
		<input type="text" name="cdate"/>
		<br/>
		ctime:&nbsp;&nbsp;<br/>
		<input type="text" name="ctime"/>
		<br/>
		location:&nbsp;&nbsp;<br/>
		<input type="text" name="location"/>
		<br/>
		price:&nbsp;&nbsp;<br/>
		<input type="text" name="price"/>
		<br/>
		vehid:&nbsp;&nbsp;<br/>
		<input type="text" name="vehid"/>
		<br/>
		<p>
		<input type="submit" value="Submit"/>
		&nbsp;&nbsp;
		<button type="button"><a href="mainView.php">Cancel</a></button>
		</p>
	</fieldset>
	</form>
	
	</div>
	
	<div id="footer">
	<p style="text-align:center"><?php getDisclaimer(); ?></p>
	</div>
	
	</body>
</html>
