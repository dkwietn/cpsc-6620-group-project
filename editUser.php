<!DOCTYPE html>
<html>
	<head>
		<link type="text/css" rel="stylesheet" href="stylesheet.css"/>
		<title>Parking Violations</title>
	</head>
	<body>
	
	<?php require_once('query.php'); ?>
	
	<div id="header">
		<h1 style="text-align:left">Edit Account</h1>
	</div>
	
	<?php
		require_once('query.php');
		makeUserHeader();
	?>
	
	<p/>
	
	<?php 
	//Session variable to get value for users that have logged in
	if(!isset($_SESSION)) {
		session_start();
	}
	// variables used to populate user information fields
	$tableName = 'user';
	$columnID = 'username';

	// check if user is editing own account or if admin is editing another account
	if(isset($_SESSION["UserLevel"]) && $_SESSION["UserLevel"] === "ADMIN" && isset($_GET["username"] ) )
	{
		$value = $_GET["username"];
	}
	else
	{
		$value = $_SESSION["User"];
	}
	
	?>
	
	<div id="section">
	<form action="<?php echo 'updateUser.php?username=' . $value ?>" method="post">
	<fieldset>
		Username:<span syle="color:red">*</span>&nbsp;&nbsp;<br/>
		<input type="text" name="username" value="<?php echo getSingleQueryColumn( $tableName, 'username', $columnID, $value ); ?>"/>
		<br/>
		First Name:&nbsp;&nbsp;<br/>
		<input type="text" name="first_name" value='<?php echo getSingleQueryColumn( $tableName, "fname", $columnID, $value ); ?>'/>
		<br/>
		Last Name:&nbsp;&nbsp;<br/>
		<input type="text" name="last_name" value='<?php echo getSingleQueryColumn( $tableName, "lname", $columnID, $value ); ?>'/>
		<br/>
		Age:&nbsp;&nbsp;<br/>
		<input type="text" name="age" value='<?php echo getSingleQueryColumn( $tableName, "age", $columnID, $value ); ?>'/>
		<br/>
		Telephone Number:&nbsp;&nbsp;<br/>
		<input type="text" name="telephone_number" value='<?php echo getSingleQueryColumn( $tableName, "phoneNum", $columnID, $value ); ?>'/>
		<br/>
		Email Address:&nbsp;&nbsp;<br/>
		<input type="text" name="email_address" value='<?php echo getSingleQueryColumn( $tableName, "email", $columnID, $value ); ?>'/>
		<br/>
		Mailing Address:&nbsp;&nbsp;<br/>
		<input type="text" name="mailing_address" value='<?php echo getSingleQueryColumn( $tableName, "address", $columnID, $value ); ?>'/>
		<br/>
		Password:<span syle="color:red">*</span>:&nbsp;&nbsp;<br/>
		<input id="password" type="password" name="password" onkeyup="isValidPassword(this.value)"/>
		<span id="inputPassword">
		</span>
		<br/>
		Confirm Password:<span syle="color:red">*</span>&nbsp;&nbsp;<br/>
		<input type="password" name="confirm_password" onkeyup="doPasswordsMatch( this.value, document.getElementById('password').value )"/>
		<span id="confirmPassword">
		</span>
		<p>
		<input type="submit" value="Update"/>
		&nbsp;
		<button type="button"><a href="mainView.php">Cancel</a></button>
		</p>
		<button type="button"><a href="<?php echo 'deleteUser.php?username=' . $value ?>">Delete Account</a></button>

	</fieldset>
	</form>
	
	</div>

	
	<div id="footer">
	<p style="text-align:center"><?php getDisclaimer(); ?></p>
	</div>
	
	<script>
	// display error message if password does not contain a capital letter
	function isValidPassword(password)
	{
		var passwordRegex = /[A-Z]/;
		
		if( password.search( passwordRegex ) < 0 )
		{
			document.getElementById("inputPassword").innerHTML = 'Must contain at least one capital letter';
		}
		else
		{
			document.getElementById("inputPassword").innerHTML = '';
		}
	}
	
	// display error message if password fields do not match
	function doPasswordsMatch(password, confirm_password)
	{
		if( password !== confirm_password )
		{
			document.getElementById("confirmPassword").innerHTML = 'passwords do not match';
		}
		else
		{
			document.getElementById("confirmPassword").innerHTML = '';
		}
	}
	</script>
	
	</body>
</html>
