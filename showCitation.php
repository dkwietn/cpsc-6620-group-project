<!DOCTYPE html>
<html>
	<head>
		<link type="text/css" rel="stylesheet" href="stylesheet.css"/>
		<title>Parking Violations</title>
	</head>
	<body>
	
	<?php require_once('query.php'); ?>
	
	<div id="header">
		<h1 style="text-align:left">Citation Information</h1>
	</div>
	
	<?php
		require_once('query.php');
		makeUserHeader();
	?>
	
	<?php 
	// show detailed information about a citation
	$citTable = 'citation';
	$userTable = 'customer';
	$vehTable = 'vehicle';

	$columnID = 'citnum';
	$value = $_GET["citnum"];
	?>
		
	<div id="section">
		<p style="font-size:150%">Information for Citation:&nbsp;&nbsp;
		<?php echo getSingleQueryColumn($citTable, 'citnum', $columnID, $value); ?></p>
	
	</div>

	<div id="resultsTableCitation">
		<table id="citTable" border="1px" align="left" class="sortable">

		<?php 
			// show the citation with specified citation number
			$sql = "SHOW COLUMNS FROM citation";
			$result = querySQL($sql);
			printSQLHeaders( $result );
			
			$result = selectFromWhere($citTable, $columnID, $value);
			printSQLRows($result);

		?>
		</table>
	</div>
	
	<div id="updateStuff">
	</p></br></br></br></br></p>
	<p><button id="update" type="button" style="display:none"><a href="<?php echo 'editCitation.php?citnum=' . $value ?>">Update Citation</a></button>
 	&nbsp;&nbsp;<button id="delete" type="button" style="display:none"><a href="<?php echo 'deleteCitation.php?citnum=' . $value ?>">Delete Citation</a></button></p>

	<?php 
		if(!isset($_SESSION)) {
			session_start();
		}
		$level = $_SESSION["UserLevel"]; 
	?> 
	<script>
		// allow citation to be updated or deleted if user is an admin
		var del = document.getElementById("delete");
		var update = document.getElementById("update");

		if("<?php echo $level; ?>" === "ADMIN"){
			del.style.display = 'inline';
			del.style.visibility = 'visible';
			update.style.display = 'inline';
			update.style.visibility = 'visible';
		}else{
			del.style.display = 'none';
			del.style.visibility = 'hidden';
			update.style.display = 'none';
			update.style.visibility = 'hidden';
		}
	</script>
	</div>

	<p></p>
	<p style="font-size:150%">Associated Customer and Vehicle:&nbsp;&nbsp;
	</p>

	<div id="resultsTableUser">
		<table id="userTable" border="1px" align="left" class="sortable">
		<thead>
			<th>custid</th>
			<th>classification</th>
			<th>vehid</th>
			<th>make</th>
			<th>color</th>
		</thead>

		<?php 
		// show the vehicle associated with the ciation
		$result = joinCustVeh($value);
		printSQLRows($result);
		//makeTablesSortable();

		?>

		<script>
		//make row clickable because there are multiple tables
		var clink = '<a href=showCustomer.php?custid=';
		var vlink = '<a href=showVehicle.php?vehid=';
		var cidx = 0;
		var vidx = 0;
		var table = document.getElementById("userTable");
		//find header index
		for (var i = 0, col; col = table.rows[0].cells[i]; i++) 
		{
			if( col.innerHTML === "custid" )
			{
				cidx = i;
			}
			if( col.innerHTML === "vehid" )
			{
				vidx = i;
			}
		}  
		
		// add links to rows
		for (var i = 1, row; row = table.rows[i]; i++)
		{
			row.cells[cidx].innerHTML = clink + row.cells[cidx].innerHTML + '>' + row.cells[cidx].innerHTML + '</a>';
			row.cells[vidx].innerHTML = vlink + row.cells[vidx].innerHTML + '>' + row.cells[vidx].innerHTML + '</a>';
			
		}

		</script>
		</table>
	</br></br>
	</div>

	<div id="footer">
	<p style="text-align:center"><?php getDisclaimer(); ?></p>
	</div>
	</body>
</html>
