<!DOCTYPE html>
<html>
	<head>
		<link type="text/css" rel="stylesheet" href="stylesheet.css"/>
		<title>Parking Violations</title>
	</head>
	<body>
	
	<?php require_once('query.php'); ?>
	
	<div id="header">
		<h1 style="text-align:left">Vehicle Information</h1>
	</div>
	
	<?php
		require_once('query.php');
		makeUserHeader();
	?>
	
	<?php 
	// show detailed information about a vehicle
	$citTable = 'citation';
	$userTable = 'customer';
	$vehTable = 'vehicle';

	$columnID = 'vehid';
	$id = $_GET["vehid"];
	?>
		
	<div id="section">
		<p style="font-size:150%">Information for Vehicle:&nbsp;&nbsp;
		<?php echo $id ?></p>
	
	</div>

	<div id="resultsTableVehicle">
		<table id = "vehTable" border="1px" align="left" class="sortable">

		<?php 
		// show vehicle information
		$sql = "SHOW COLUMNS FROM vehicle";
		$result = querySQL($sql);
		printSQLHeaders($result);

		$sql = "SELECT * FROM vehicle WHERE vehid = '" . $id . "'";
		$result = querySQL($sql);
		printSQLRows($result);
		?>

	</table>
	</div>

	<div id="updateStuff">
	</p></br></br></br></br></p>
	<p><button id="update" type="button" style="display:none"><a href="<?php echo 'editVehicle.php?vehid=' . $id ?>">Update Vehicle</a></button></p>

	<?php 
		if(!isset($_SESSION)) {
			session_start();
		}
		$level = $_SESSION["UserLevel"]; 
	?> 
	<script>
		// allow the vehicle information to be updated if the user is an admin
		var update = document.getElementById("update");

		if("<?php echo $level; ?>" === "ADMIN"){
			update.style.display = 'inline';
			update.style.visibility = 'visible';
		}else{
			update.style.display = 'none';
			update.style.visibility = 'hidden';
		}
	</script>
	</div>
	
	<p></p>
	<p style="font-size:150%">Associated Customer:&nbsp;&nbsp;
	</p>
	<div id="resultsTableUser">
		<table id = "custTable" border="1px" align="left" class="sortable">
		<thead>
			<th>custid</th>
			<th>classification</th>
		</thead>

		<?php 
		// show the customer owning the vehicle
		$sql = "SELECT * FROM customer WHERE custid IN (SELECT custid FROM vehicle where vehid = '" . $id . "')";
		$result = querySQL($sql);
		printSQLRows($result);
		?>
		</table>

		<script>
		//make row clickable because there are multiple tables
		var link = '<a href=showCustomer.php?custid=';
		var idx = 0;
		var table = document.getElementById("custTable");
		//find header index
		for (var i = 0, col; col = table.rows[0].cells[i]; i++) 
		{
			if( col.innerHTML === "custid" )
			{
				idx = i;
				break;
			}
		}  
		// add links to rows
		for (var i = 1, row; row = table.rows[i]; i++)
		{
			row.cells[idx].innerHTML = link + row.cells[idx].innerHTML + '>' + row.cells[idx].innerHTML + '</a>';
		}

		</script>
	</br></br></br>
	</div>

	

	<p></br></p>
	<p style="font-size:150%">Associated Citations:&nbsp;&nbsp;
	</p>

	<div id="resultsTableCitation">
		<table id ="citTable" border="1px" align="left" class="sortable">

		<?php 
			// show all citations associated with the vehicle
			$sql = "SHOW COLUMNS FROM citation";
			$result = querySQL($sql);
			printSQLHeaders( $result );
			
			$sql = "SELECT * FROM citation WHERE vehid = '" . $id . "'"; 
			$result = querySQL($sql);
			printSQLRows($result);
		?>

		<script>
		//make row clickable because there are multiple tables
		var link = '<a href=showCitation.php?citnum=';
		var idx = 0;
		var table = document.getElementById("citTable");
		//find header index
		for (var i = 0, col; col = table.rows[0].cells[i]; i++) 
		{
			if( col.innerHTML === "citNum" )
			{
				idx = i;
				break;
			}
		}  
		
		// add links to rows
		for (var i = 1, row; row = table.rows[i]; i++)
		{
			row.cells[idx].innerHTML = link + row.cells[idx].innerHTML + '>' + row.cells[idx].innerHTML + '</a>';
		}

		</script>
		</table>
	</div>
	
	</br></br></br>

	
	
	<div id="footer">
	</br></br>
	<p style="text-align:center"><?php getDisclaimer(); ?></p>
	</div>
	
	</body>
</html>
