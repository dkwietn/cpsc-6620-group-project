<!DOCTYPE html>
<html>
	<head>
		<link type="text/css" rel="stylesheet" href="stylesheet.css"/>
		<title>Parking Violations</title>
	</head>
	<body>
	
	<?php require_once('query.php'); ?>
	
	<div id="header">
		<h1 style="text-align:left">Customer Information</h1>
	</div>
	
	<?php
		require_once('query.php');
		makeUserHeader();
	?>
	
	<?php 
	// show detailed information about a customer
	$citTable = 'citation';
	$userTable = 'customer';
	$vehTable = 'vehicle';

	$columnID = 'custid';
	$id = $_GET["custid"];
	?>
		
	<div id="section">
		<p style="font-size:150%">Information for Customer:&nbsp;&nbsp;
		<?php echo $id ?></p>

		<div id="resultsTableUser">
			<table border="1px" align="left" class="sortable">
			<thead>
				<th>custid</th>
				<th>classification</th>
			</thead>

			<?php 
			// show information about the customer
			$sql = "SELECT * FROM customer WHERE custid = '" . $id . "'";
			$result = querySQL($sql);
			printSQLRows($result);
			?>
			</table>
		</div>

		<div id="updateStuff">
		</p></br></br></br></br></p>
		<p><button id="update" type="button" style="display:none"><a href="<?php echo 'editCustomer.php?custid=' . $id ?>">Update Customer</a></button></p>

		<?php 
			if(!isset($_SESSION)) {
				session_start();
			}
			$level = $_SESSION["UserLevel"]; 
		?> 
		<script>
			// allow customer information to be updated if user is admin
			var update = document.getElementById("update");

			if("<?php echo $level; ?>" === "ADMIN"){
				update.style.display = 'inline';
				update.style.visibility = 'visible';
			}else{
				update.style.display = 'none';
				update.style.visibility = 'hidden';
			}
		</script>
		</div>
		
		<p></p>
		<p style="font-size:150%">Associated Vehicles:&nbsp;&nbsp;
		</p>
		<div id="resultsTableVehicle">
			<table id = "vehTable" border="1px" align="left" class="sortable">

			<?php 
			// show vehicles belonging to the customer
			$sql = "SHOW COLUMNS FROM vehicle";
			$result = querySQL($sql);
			printSQLHeaders($result);

			$sql = "SELECT * FROM vehicle WHERE custid = '" . $id . "'";
			$result = querySQL($sql);
			printSQLRows($result);
			?>

			<script>
			
			//make row clickable because there are multiple tables
			var link = '<a href=showVehicle.php?vehid=';
			var idx = 0;
			var table = document.getElementById("vehTable");
			//find header index
			for (var i = 0, col; col = table.rows[0].cells[i]; i++) 
			{
				if( col.innerHTML === "vehid" )
				{
					idx = i;
					break;
				}
			}  
			// add links to rows
			for (var i = 1, row; row = table.rows[i]; i++)
			{
				row.cells[idx].innerHTML = link + row.cells[idx].innerHTML + '>' + row.cells[idx].innerHTML + '</a>';
			}

			</script>
			</table>
		</div>

		</p></br></br></br></br></p>
		<p style="font-size:150%">Associated Citations:&nbsp;&nbsp;
		</p>

		<div id="resultsTableCitation">
			<table id ="citTable" border="1px" align="left" class="sortable">

			<?php 
				// show all citations a customer has
				$sql = "SHOW COLUMNS FROM citation";
				$result = querySQL($sql);
				printSQLHeaders( $result );
				
				$result = joinCustCit($id);
				printSQLRows($result);
			?>

			<script>
			//make row clickable because there are multiple tables
			var link = '<a href=showCitation.php?citnum=';
			var idx = 0;
			var table = document.getElementById("citTable");
			//find header index
			for (var i = 0, col; col = table.rows[0].cells[i]; i++) 
			{
				if( col.innerHTML === "citNum" )
				{
					idx = i;
					break;
				}
			}  
			
			// add links to rows
			for (var i = 1, row; row = table.rows[i]; i++)
			{
				row.cells[idx].innerHTML = link + row.cells[idx].innerHTML + '>' + row.cells[idx].innerHTML + '</a>';
			}

			</script>
			</table>
		</br></br></br>
		</div>
	</div>

	</br>
	
	<div id="footer">
	</br></br>
	<p style="text-align:center"><?php getDisclaimer(); ?></p>
	</div>
	
	</body>
</html>
