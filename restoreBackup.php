<!DOCTYPE html>
<html>
	<head>
		<link type="text/css" rel="stylesheet" href="stylesheet.css"/>
		<title>Parking Violations</title>
	</head>
	<body>
	<div id="header">
		<h1 style="text-align:center">Welcome to our Database of Clemson University Parking Violations</h1>
	</div>
	
	<?php
		require_once('query.php');
		makeUserHeader();
	?>

	<h2>Select Backup File to Restore From</h2>
	<form  enctype="multipart/form-data" action="restoreTable.php" method="post">
	<input type="hidden" name="MAX_FILE_SIZE" value="500000" />
	<input type="file" name="backupFile" />
	<br>
	<input type="submit" value="Submit">
	</form>
	
	</body>
</html>