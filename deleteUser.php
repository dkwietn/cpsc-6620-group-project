<!DOCTYPE html>
<html>
	<head>
		<link type="text/css" rel="stylesheet" href="stylesheet.css"/>
		<title>Parking Violations</title>
	</head>
<?php
require_once('query.php');

// delete user from user table
$username = $_GET["username"];
deleteRecord( 'user', 'username', $username );

if(!isset($_SESSION)) {
	session_start();
}

// redirect user to manage account page if admin or login page if not
echo "<div id='confirm'><p>User Deleted</p></div>";
if($_SESSION["UserLevel"] === "ADMIN"){
	if($username != $_SESSION["Username"])
		echo "<div id ='continue'><a href='manageUserAccounts.php'>Click Here</a></div>";
	else{
		removeCookie();
		session_unset();
		session_destroy();
		echo "<div id ='continue'><a href='index.html'>Click Here</a></div>";
	}
}else{
	removeCookie();
	session_unset();
	session_destroy();
	echo "<div id ='continue'><a href='index.html'>Click Here</a></div>";
}

?>
</html>