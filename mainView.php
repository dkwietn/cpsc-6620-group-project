<!DOCTYPE html>
<html>
	<head>
		<link type="text/css" rel="stylesheet" href="stylesheet.css"/>
		<title>Parking Violations</title>
	</head>
	<body>
	<div id="header">
		<h1 style="text-align:center">Welcome to our Database of Clemson University Parking Violations</h1>
		<script src="scripts.js"></script>
	</div>
	
	<?php
		require_once('query.php');
		makeUserHeader();
	?>

	<p><button id = "backup" type="button" style="display:none"><a href="makeBackup.php">Make Backup</a></button>
	&nbsp;&nbsp;<button id = "restore" type="button" style="display:none"><a href="restoreBackup.php">Recover from Backup</a></button></p>
	
	<div id="section">
		<p>Select Table:</p>
		<select id = "searchTable" onchange = "updateFields(this)">
			<option value="citation" selection="selected">Citation</option>
			<option value="customer">Customer</option>
			<option value="vehicle">Vehicle</option>
		</select>


		
		<p>Search By:</p>
		<!-- Citations search -->
		<select id = "searchCit" style="display:block" onchange = "setValue(this)">
			<option value="citnum" selection="selected">Citation#</option>
			<option value="cdate">Date(yyyy-mm-dd)</option>
			<option value="description">Description</option>
			<option value="location">Location</option>
			<option value="price">Price</option>
			<option value="ctime">Time</option>
		</select>

		<script>
		document.getElementById('searchTable').value="citation";
		document.getElementById('searchCit').value="citnum";
		</script>
		
		<!-- Vehicle search -->
		<select id = "searchVeh" style="display:none" onchange= "setValue(this)">
			<option value="vehid" selection="selected">Vehcile ID</option>
			<option value="model">Make</option>
			<option value="color">Color</option>
		</select>

		<!-- Customer Search -->
		<select id = "searchCust" style="display:none" onchange= "setValue(this)">
			<option value="custid" selection="selected">Customer ID</option>
			<option value="classification">Classification</option>
		</select>

		<!-- Search values -->
		<input id = "search" value="citnum" style="display:none">
		<input id= "table" value="citation" style="display:none"> 

		<p><input id="searchDatabase" type="text"/> <button type="button" onclick="searchDatabase(document.getElementById('resultsTable'), 1, 25, document.getElementById('searchDatabase').value, document.getElementById('table').value, document.getElementById('search').value)">Search</button></p>
		<!--- 
		<p><button type="button"><a href="#">Update Selected Parking Violation</a></button></p>
		<p><button type="button"><a href="#">Delete Selected Parking Violation(s)</a></button></p>
		-->
		<div id = "filt" style="display:block">
		<p>Filter Last <input id='dayFilter' type="number" name="days" min="1" max="999"> day(s) <button type="button" onclick="filterLastNDays(document.getElementById('resultsTable'), 1, 25, document.getElementById('dayFilter').value, document.getElementById('search').value, document.getElementById('searchDatabase').value )">Filter</button></p>
		</div>
		<p><button id = "make" type="button" style="display:none"><a href="makeCitation.php">Add Parking Violation</a></button></p>
		<p><button type="button" onclick="showAll(document.getElementById('resultsTable'), 1, 25, document.getElementById('table').value)">Show All</button></p>
		<div id="resultsTable">
	
		</div>
	
		<script>
			//Change search fields based on selected table
			function updateFields(selection) {
				var veh = document.getElementById("searchVeh");
				var cit = document.getElementById("searchCit");
				var cust = document.getElementById("searchCust");
				var table = document.getElementById("table");
				var search = document.getElementById("search");
				var filt = document.getElementById("filt");
				var makeCitation = document.getElementById("make");

				if(selection.value == "citation") {
					cit.style.display = 'block';
					cit.style.visibility = 'visible';
					cust.style.display = 'none';
					cust.style.visibility = 'hidden';
					veh.style.display = 'none';
					veh.style.visibility = 'hidden';
					filt.style.display = 'block';
					filt.style.visibility = 'visible';
					table.value = "citation";
					search.value = "citnum";
					makeCitation.style.display = 'block';
					makeCitation.style.visibility = 'visible';
				}else if(selection.value == "customer") {
					cit.style.display = 'none';
					cit.style.visibility = 'hidden';
					cust.style.display = 'block';
					cust.style.visibility = 'visible';
					veh.style.display = 'none';
					veh.style.visibility = 'hidden';
					filt.style.display = 'none';
					filt.style.visibility = 'hidden';
					table.value = "customer";
					search.value = "custid";
					makeCitation.style.display = 'none';
					makeCitation.style.visibility = 'hidden';
				}else if(selection.value == "vehicle") {
					cit.style.display = 'none';
					cit.style.visibility = 'hidden';
					cust.style.display = 'none';
					cust.style.visibility = 'hidden';
					veh.style.display = 'block';
					veh.style.visibility = 'visible';
					filt.style.display = 'none';
					filt.style.visibility = 'hidden';
					table.value = "vehicle";
					search.value = "vehid";
					makeCitation.style.display = 'none';
					makeCitation.style.visibility = 'hidden';
				}
			}

			//Set the search value
			function setValue(selection) {
				var search = document.getElementById("search");
				search.value = selection.value;
			}
		</script>

		<?php 
			// get user level
			if(!isset($_SESSION)) {
				session_start();
			}
			$level = $_SESSION["UserLevel"]; 
		?> 


		<script>
			// Hide fields based on user level
			var make = document.getElementById("make");
			var backup = document.getElementById("backup");
			var restore = document.getElementById("restore");

			if("<?php echo $level; ?>" === "ADMIN"){
				make.style.display = 'block';
				make.style.visibility = 'visible';
				backup.style.display = 'inline';
				backup.style.visibility = 'visible';
				restore.style.display = 'inline';
				restore.style.visibility = 'visible';
			}else{
				make.style.display = 'none';
				make.style.visibility = 'hidden';
				backup.style.display = 'none';
				backup.style.visibility = 'hidden';
				restore.style.display = 'none';
				restore.style.visibility = 'hidden';
			}
		</script>

	</div>
	
	<div id="footer">
	<p style="text-align:center"><?php getDisclaimer(); ?></p>
	</div>	
	</body>
</html>
