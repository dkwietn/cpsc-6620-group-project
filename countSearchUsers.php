<?php
// count number of records in user table that meet search criteria for search button
require_once('query.php');
$search = $_GET["search"]; // search term
$whereClause = makeMultiColumnQuery( "user", $search );
$sql = "SELECT COUNT(*) FROM user WHERE " . $whereClause;
$result = querySQL( $sql );
$row = $result->fetch_row();
// echo number of records
echo $row[0];
?>