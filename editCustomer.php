<!DOCTYPE html>
<html>
	<head>
		<link type="text/css" rel="stylesheet" href="stylesheet.css"/>
		<title>Parking Violations</title>
	</head>
	<body>
	
	<?php require_once('query.php'); ?>
	
	<div id="header">
		<h1 style="text-align:left">Edit Customer</h1>
	</div>
	
	<?php
		require_once('query.php');
		makeUserHeader();
	?>
	
	<p>
	<?php 
	// variables used to populate customer information fields
	$tableName = 'customer';
	$columnID = 'custid';
	$value = $_GET["custid"];
	?>
		
	<div id="section">
	<form action="<?php echo 'updateCustomer.php?custid=' . $value ?>"  method="post">
	<fieldset>
		custid:&nbsp;&nbsp;<br/>
		<input id = "new" type="text" name="custid" value="<?php echo getSingleQueryColumn( $tableName, 'custid', $columnID, $value ); ?>"/>
		<br/>
		classification:&nbsp;&nbsp;<br/>
		<input type="text" name="classification" value='<?php echo getSingleQueryColumn( $tableName, "classification", $columnID, $value ); ?>'/>
		<br/>
		<p>
		<input type="submit" value="Update"/>
		&nbsp;&nbsp;
		<button type="button"><a href="<?php echo 'showCustomer.php?custid=' . $value ?>">Cancel</a></button>
		</p>
	</fieldset>
	</form>
	
	</div>
	
	<div id="footer">
	<p style="text-align:center"><?php getDisclaimer(); ?></p>
	</div>
	
	</body>
</html>