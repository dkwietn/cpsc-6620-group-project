<?php

// set user cookie
function setUserCookie($username, $userLevel)
{
	setcookie("User", $username, time() + (86400 * 1), "/"); // 86400 = 1 day
	setcookie("UserLevel", $userLevel, time() + (86400 * 1), "/"); // 86400 = 1 day
}

// delete user cookie
function removeCookie() {
	unset($_COOKIE["User"]);
	unset($_COOKIE["UserLevel"]);
	//setcookie("User", "", time()-3600);
	//setcookie("UserLevel", "", time()-3600);
}

// return account level based on cookie
function getUserLevelFromCookies()
{
	if( !array_key_exists('userLevel', $_COOKIE ) )
		return 0;
	
	switch($_COOKIE['userLevel'])
	{
	case "NOACCOUNT":
		return 0;
	case "NORMAL":
		return 1;
	case "ADMIN":
		return 2;
	default:
		return 0;
	}
}

// get user level from username and password combination
function getUserLevel($username, $password)
{
	$conn = getSQLConnection();
	$sql = "SELECT isAdmin FROM user WHERE username=? AND password=?";
	$stmt = $conn->stmt_init();
	$password = hash( 'md5', $password );
	if( !$stmt->prepare($sql) )
	{
		$rValue = "NOACCOUNT";
	}
	else
	{
		$stmt->bind_param('ss', $username, $password);
		$stmt->execute();
		$stmt->bind_result($result);
		if( $stmt->fetch() )
		{
			if( $result === 1 )
				$rValue = "ADMIN";
			else $rValue = "NORMAL";
		}
		else
		{
			$rValue = "NOACCOUNT";
		}
	}
	
	$stmt->close();
	$conn->close();
	return $rValue;
	// retrieve using $_COOKIE['userLevel']
}

// get first name based on username and password
function getUserName($username, $password) 
{
	$conn = getSQLConnection();
	$sql = "SELECT fname FROM user WHERE username=? AND password=?";
	$stmt = $conn->stmt_init();
	$password = hash( 'md5', $password );
	$rValue = $username;
	if(!$stmt->prepare($sql) )
	{
		$stmt->bind_param('ss', $username, $password);
		$stmt->execute();
		$stmt->bind_result($result);
		$stmt->fetch();

		if($result  && $result != '')
			$rValue = $result;
	}
	
	$stmt->close();
	$conn->close();
	return $rValue;
}

// perform an sql query specified by $query string
function querySQL($query){
	$conn = getSQLConnection();
	if ($conn->connect_error)
	{
		return "Connection failed: " . $conn->connect_error;
	}
	$result = $conn->query($query);
	$conn->close();
	return $result;
}

// return the value of column name when column id matches value in table name
function getSingleQueryColumn( $tableName, $columnName, $columnID, $value )
{
	$query = "SELECT " . $columnName . " FROM " . $tableName . " WHERE " . $columnID . " = '" . $value . "'";
	$result = querySQL( $query );
	$row = $result->fetch_row();
	if( $row )
		return $row[0];
	else
		return "";
}

// delete a record from a table where the column id is equal to the unique key
function deleteRecord( $tableName, $columnID, $uniqueKey )
{
	$query = "DELETE FROM " . $tableName . " WHERE " . $columnID . " = '" . $uniqueKey . "'";
	$result = querySQL( $query );
}

// get a connection to our sql database
function getSQLConnection()
{$u="cpsc462-f15";$s="mysql1.cs.clemson.edu";
$p="getmein12";$d="xinyil-cpsc462_cix4";
$conn = new mysqli($s, $u, $p, $d);
return $conn;}

// print table headers based on a "describe table" query's result
function printSQLHeaders( $result )
{
	if( $result->num_rows > 0 )
	{
		echo '<thead>';
		while ($row = $result->fetch_row())
		{
			echo "<th>$row[0]</th>";
		}
		echo '</thead>';
	}
}

// print table rows based on a "select" query's result
function printSQLRows( $result )
{
	echo '<tbody>';
	while( $row = $result->fetch_row() )
	{
		echo '<tr>';
		foreach ($row as $attribute)
		{
			echo "<td>$attribute</td>";
		}
		echo '</tr>';
	}
	echo '</tbody>';
}

// return a table for the current page with the size using the query string
function queryGetPage( $query, $page, $size )
{
	$result = querySQL( "$query LIMIT " . (($page - 1) * $size) . " , $size" );
	printSQLRows( $result );
}

// return array of all database tables
function getTables()
{
	$sql = "SHOW TABLES";
	$result = querySQL($sql);
	$tableArray = array();
	if( $result->num_rows > 0 )
	{
		while ($row = $result->fetch_row())
		{
			array_push( $tableArray, $row[0] );
		}
	}
	
	return $tableArray;
}

// return array containing table headers
function getSQLHeaders( $tableName )
{
	$sql = "SHOW COLUMNS FROM " . $tableName;
	$result = querySQL($sql);
	$headerArray = array();
	if( $result->num_rows > 0 )
	{
		while ($row = $result->fetch_row())
		{
			array_push( $headerArray, $row[0] );
		}
	}
	
	return $headerArray;
}

// return 2d array of records
function getSQLData( $tableName )
{
	$sql = "SELECT * FROM " . $tableName;
	$result = querySQL( $sql );
	$dataArray = array();
	while( $row = $result->fetch_row() )
	{	
		$recordArray = array();
		foreach ($row as $attribute)
		{
			array_push( $recordArray, $attribute );
		}
		array_push( $dataArray, $recordArray );
	}
	return $dataArray;
}

// subtract number of days from last citation added date
function subtractFromToday( $days )
{
	$date = date_create('2015-08-13');
	$date = date_sub($date, date_interval_create_from_date_string($days . ' days'));
	return "'" . date_format($date, 'Y-m-d') . "'";
}

// search all columns of a table for the search query
function makeMultiColumnQuery( $tableName, $searchQuery )
{
	$whereClause = "TRUE";
	if( $searchQuery != "")
	{
		$sql = "SHOW COLUMNS FROM " . $tableName;
		$result = querySQL($sql);
		$searchQuery =  "'%" . $searchQuery . "%'";
		if( $result->num_rows > 0 )
		{
			$row = $result->fetch_row();
			$whereClause = $row[0] . " LIKE " . $searchQuery . "";
			while ($row = $result->fetch_row())
			{
				$whereClause = $whereClause . " OR " . $row[0] . " LIKE " . $searchQuery . "";
			}
		}
	}
	return $whereClause;
}

// search a single column of a table for the search query
function makeSingleColumnQuery( $tableName, $column, $searchQuery )
{
	$whereClause = "TRUE";
	if( $searchQuery != "")
	{
		$searchQuery =  "'%" . $searchQuery . "%'";
		$whereClause = $column . " LIKE " . $searchQuery . "";
	}
	return $whereClause;
}

// join customer and vehicle tables to return  vehicle associated with the citation number
function joinCustVeh($value) 
{
	$query = "SELECT * FROM (customer NATURAL JOIN vehicle) WHERE custid IN (SELECT custid FROM  vehicle NATURAL JOIN citation where citation.citnum = '" . $value . "')";
	$result = querySQL($query);

	if($result)
		return $result;
	else 
		return "";
}

// join customer and citation  tables and return citation records associated with specified custid value
function joinCustCit($value) 
{
	$query = "SELECT * FROM citation WHERE vehid IN (SELECT vehid FROM  (vehicle NATURAL JOIN customer) where customer.custid = '" . $value . "')";
	$result = querySQL($query);

	if($result)
		return $result;
	else 
		return "";
}

// return results where the column id matches the value in the table
function selectFromWhere($table, $column, $value) {
	$sql = "SELECT * FROM " . $table . " WHERE " . $column . " = '" . $value . "'";
	$result = querySQL($sql);

	if($result)
		return $result;
	else
		return "";
}

// print the rows associated with a sql query
function echoQueryResults( $sql )
{
	$result = querySQL($sql);
	printSQLRows( $result );	
}

// make the html header for a user depending on login status
function makeUserHeader()
{   
	//Logged in
	if(!isset($_SESSION)) {
		session_start();
	}

	//If the user is logged in, go to home page. If not, log in
	if(isset($_SESSION["UserLevel"]))  {
		$page = "mainView.php";
		//Show different headers for user or admin
		if(isset($_SESSION["UserLevel"]) && $_SESSION["UserLevel"] === "ADMIN") {
			echo"<div id='header'> 
					<div style='float: left'> 
						<u><a href='$page'>Home</a></u> | <u><a href='manageUserAccounts.php'>Manage Users</a></u>
			 			| <u><a href='about.php'>About</a></u> | <u><a href='contactUs.php'>Contact Us</a></u>
					</div>
					<div id = 'login-status' style='float:right'><u><a href='editUser.php'>Manage Account</a></u> 
						| <a href='logout.php'>Logout</a>
					</div>
					<div style='clear:both'></div>
				</div>";
		}else{
			echo"<div id='header'> 
					<div style='float: left'><u><a href='$page'>Home</a></u> | 
						<u><a href='about.php'>About</a></u> | <u><a href='contactUs.php'>Contact Us</a></u>
					</div>
					<div id= 'login-status' style='float:right;'<u><a href='editUser.php'>Manage Account</a></u> | <a href='logout.php'>Logout</a>
					</div>
					<div style='clear:both'></div>
				</div>";
		}
	}else{
		$page = "index.html";
		echo"<div id='header'> 
					<div style='float: left'><u><a href='$page'>Home</a></u> | 
						<u><a href='about.php'>About</a></u> | <u><a href='contactUs.php'>Contact Us</a></u>
				</div>
					</div>
					<div style='clear:both'></div>
				</div>";
	}
}

// get the disclaimer to print in the footer
function getDisclaimer()
{
echo 'Disclaimer: This website was created for CPSC 4620/6620 Fall 2015 at Clemson University and may not display accurate real-world data.';
}

?>
