<!DOCTYPE html>
<html>
	<head>
		<link type="text/css" rel="stylesheet" href="stylesheet.css"/>
		<title>Parking Violations</title>
	</head>
	<body>
	
	<?php require_once('query.php'); ?>
	
	<div id="header">
		<h1 style="text-align:left">Edit Citation</h1>
	</div>
	
	<?php
		require_once('query.php');
		makeUserHeader();
	?>
	
	<p>
	<?php 
	// variables used to populate citation information fields
	$tableName = 'citation';
	$columnID = 'citnum';
	$value = $_GET["citnum"];
	?>
		
	<div id="section">
	<form action="<?php echo 'updateCitation.php?citnum=' . $value ?>"  method="post">
	<fieldset>
		citnum:&nbsp;&nbsp;<br/>
		<input id = "new" type="text" name="citnum" value="<?php echo getSingleQueryColumn( $tableName, 'citnum', $columnID, $value ); ?>"/>
		<br/>
		description:&nbsp;&nbsp;<br/>
		<input type="text" name="description" value='<?php echo getSingleQueryColumn( $tableName, "description", $columnID, $value ); ?>'/>
		<br/>
		cdate:&nbsp;&nbsp;<br/>
		<input type="text" name="cdate" value='<?php echo getSingleQueryColumn( $tableName, "cdate", $columnID, $value ); ?>'/>
		<br/>
		ctime:&nbsp;&nbsp;<br/>
		<input type="text" name="ctime" value='<?php echo getSingleQueryColumn( $tableName, "ctime", $columnID, $value ); ?>'/>
		<br/>
		location:&nbsp;&nbsp;<br/>
		<input type="text" name="location" value='<?php echo getSingleQueryColumn( $tableName, "location", $columnID, $value ); ?>'/>
		<br/>
		price:&nbsp;&nbsp;<br/>
		<input type="text" name="price" value='<?php echo getSingleQueryColumn( $tableName, "price", $columnID, $value ); ?>'/>
		<br/>
		vehid:&nbsp;&nbsp;<br/>
		<input type="text" name="vehid" value='<?php echo getSingleQueryColumn( $tableName, "vehid", $columnID, $value ); ?>'/>
		<br/>
		<p>
		<input type="submit" value="Update"/>
		&nbsp;&nbsp;
		<button type="button"><a href="<?php echo 'showCitation.php?citnum=' . $value ?>">Cancel</a></button>
		</p>
	</fieldset>
	</form>
	
	</div>
	
	<div id="footer">
	<p style="text-align:center"><?php getDisclaimer(); ?></p>
	</div>
	
	</body>
</html>