<?php
require_once('query.php');
// show all vehicle records
echo '<table id="citTable" border="1px" align="center" class="sortable">';
// print all headers
$sql = "SHOW COLUMNS FROM vehicle";
$result = querySQL($sql);
printSQLHeaders( $result );
// print paginated records
$sql = "SELECT * FROM vehicle";
queryGetPage( $sql, $_GET["page"], $_GET["size"] );
echo '</table>';
?>