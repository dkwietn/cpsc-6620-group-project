<?php
require_once('query.php');
// search user table on specified for specified value
echo '<table border="1px" align="center" class="sortable">';
// print header values
$sql = "SHOW COLUMNS FROM user";
$result = querySQL($sql);
printSQLHeaders( $result );
$search = $_GET["search"];
// search all columns to find search string
$whereClause = makeMultiColumnQuery( "user", $search );
// print records matching search string
$sql = "SELECT * FROM user WHERE " . $whereClause;
queryGetPage( $sql, $_GET["page"], $_GET["size"] );
echo '</table>';
?>