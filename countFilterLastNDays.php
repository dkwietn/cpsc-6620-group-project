<?php
// count number of citation records in last N day database for filter button
require_once('query.php');
$search = "";
// check if there is a search string to filter on
if( isset($_GET['column']) && isset($_GET['searchString']) )
{
	$search = " AND " . makeSingleColumnQuery( 'citation', $_GET['column'], $_GET['searchString']);
}
// account for the fact that database is not up to date
$lastDate = subtractFromToday($_GET["days"]);
$sql = "SELECT COUNT(*) FROM citation WHERE cdate > " . $lastDate . $search;
$result = querySQL( $sql );
$row = $result->fetch_row();
// echo number of records
echo $row[0];
?>