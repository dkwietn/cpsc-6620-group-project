<!DOCTYPE html>
<html>
	<head>
		<link type="text/css" rel="stylesheet" href="stylesheet.css"/>
		<title>Parking Violations</title>
	</head>
	<body>
	<div id="header">
		<h1 style="text-align:left">Create Account</h1>
	</div>
	
	<?php
		require_once('query.php');
		makeUserHeader();
	?>
	
	<p/>
	
	<div id="section">
	<form action="createAcc.php" method="post">
	<fieldset>
		Username:<span syle="color:red">*</span>&nbsp;&nbsp;<br/>
		<input type="text" name="username"/>
		<br/>
		First Name:&nbsp;&nbsp;<br/>
		<input type="text" name="first_name"/>
		<br/>
		Last Name:&nbsp;&nbsp;<br/>
		<input type="text" name="last_name"/>
		<br/>
		Age:&nbsp;&nbsp;<br/>
		<input type="text" name="age"/>
		<br/>
		Telephone Number:&nbsp;&nbsp;<br/>
		<input type="text" name="telephone_number"/>
		<br/>
		Email Address:&nbsp;&nbsp;<br/>
		<input type="text" name="email_address"/>
		<br/>
		Mailing Address:&nbsp;&nbsp;<br/>
		<input type="text" name="mailing_address"/>
		<br/>
		Password:<span syle="color:red">*</span>:&nbsp;&nbsp;<br/>
		<input id="password" type="password" name="password" onkeyup="isValidPassword(this.value)"/>
		<span id="inputPassword">
		</span>
		<br/>
		Confirm Password:<span syle="color:red">*</span>&nbsp;&nbsp;<br/>
		<input type="password" name="confirm_password" onkeyup="doPasswordsMatch( this.value, document.getElementById('password').value )"/>
		<span id="confirmPassword">
		</span>
		<p>
		<input type="submit" value="Submit"/>
		&nbsp;&nbsp;
		<button type="button"><a href="index.html">Cancel</a></button>
		</p>
	</fieldset>
	</form>
	
	</div>
	
	<div id="footer">
	<p style="text-align:center"><?php getDisclaimer(); ?></p>
	</div>
	
	<script>
	// display error message if password does not contain a capital letter
	function isValidPassword(password)
	{
		var passwordRegex = /[A-Z]/;
		
		if( password.search( passwordRegex ) < 0 )
		{
			document.getElementById("inputPassword").innerHTML = 'Must contain at least one capital letter';
		}
		else
		{
			document.getElementById("inputPassword").innerHTML = '';
		}
	}
	
	// display error message if password fields do not match
	function doPasswordsMatch(password, confirm_password)
	{
		if( password !== confirm_password )
		{
			document.getElementById("confirmPassword").innerHTML = 'passwords do not match'
		}
		else
		{
			document.getElementById("confirmPassword").innerHTML = ''
		}
	}
	</script>
	
	</body>
</html>
