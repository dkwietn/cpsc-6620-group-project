<!DOCTYPE html>
<html>
	<head>
		<link type="text/css" rel="stylesheet" href="stylesheet.css"/>
		<title>Parking Violations</title>
	</head>
	<body>
	<div id="header">
		<h1 style="text-align:center">Welcome to our Database of Clemson University Parking Violations</h1>
	</div>
	
<?php
require_once('query.php');
makeUserHeader();
// show a list of tables that can be backed up
echo "<h2>Select Table to Backup</h2>";
$tables = getTables();
foreach( $tables as $tableName )
{
	echo '<p><a href="backupTable.php?table=' . $tableName . '">' . $tableName . '</a></p>'; 
}
?>

	</body>
</html>