<!DOCTYPE html>
<html>
	<head>
		<link type="text/css" rel="stylesheet" href="stylesheet.css"/>
		<title>Parking Violations</title>
	</head>
<?php
require_once('query.php');

$wasSuccessful = 0;
$wasSuccessful = createCitation( $_POST["citnum"], $_POST["description"], $_POST["cdate"], $_POST["ctime"], $_POST["location"], $_POST["price"], $_POST["vehid"]);


// function to add a new citation into the citation database
function createCitation( $citnum, $description, $cdate, $ctime, $location, $price, $vehid)
{
	$conn = getSQLConnection();
	$sql = "INSERT INTO citation VALUES (?, ?, ?, ?, ?, ?, ?)";
	$stmt = $conn->stmt_init();
	$rValue = 0;
	if( $stmt->prepare($sql) )
	{
		if( $stmt->bind_param('sssssss', $citnum, $description, $cdate, $ctime, $location, $price, $vehid) )
		{
			if( $stmt->execute() )
			{
				$rValue = 1;
			}
		}
	}
	$stmt->close();
	$conn->close();
	return $rValue;
}

// if citation creation succeeded, redirect user to main page
// if unsuccessful return user to create citation
if( $wasSuccessful )
{
	echo "<div id='confirm'><p>Citation Added Successfully</p></div>";
	echo "<div id='continue'><a href='mainView.php'>Click Here</a></div>";
}
else
{
	echo "<div id='confirm'><p>Citation Creation Failed</p></div>";
	echo "<div id='continue'><a href='makeCitation.php'>Make Citation</a></div>";
}

?>
</html>