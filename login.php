<!DOCTYPE html>
<html>
	<head>
		<link type="text/css" rel="stylesheet" href="stylesheet.css"/>
		<title>Parking Violations</title>
	</head>
	<body>

<?php
require_once('query.php');
// get user information and set cookies to login
$userLevel = getUserLevel( $_POST["username"], $_POST["password"]);
$username = getUserName($_POST["username"], $_POST["password"]);
setUserCookie($_POST["username"], $userLevel);

if(!isset($_SESSION))
	session_start();

// display welcome message on successful login
// or allow user to create account or try again
if( $userLevel != "NOACCOUNT" )
{
	//Set user credentials for login checks
	$_SESSION["Username"] = $username;
	$_SESSION["UserLevel"] = $userLevel;
	$_SESSION["User"] = $_POST["username"];
	
	echo "<div id='confirm'> 
			<p>Login Successful</p>
		</div>";
	echo "<div id='welcome'>
			<p> Welcome, $username ($userLevel) </p>
		</div>";

	echo "<div id='continue'>
			<p><a href='mainView.php'>Click to continue</a></p>
		</div>";
}
else
{
	echo "<div id='confirm'>
			<p>Login Failed</p>
		</div>";
	echo "<div id='continue'>
			<p> <a href='createAccount.php'>Create Account</a>&nbsp
		</div>";
	echo "<p style='font-size:100%;text-align:center'> OR</p>";
	echo "<div id ='continue'>
			<p><a href='index.html'>Login</a></p>
		</div>";
}

?>

</body>
</html>