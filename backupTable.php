<!DOCTYPE html>
<html>
	<head>
		<link type="text/css" rel="stylesheet" href="stylesheet.css"/>
		<title>Parking Violations</title>
	</head>
	<body>
	<div id="header">
		<h1 style="text-align:center">Welcome to our Database of Clemson University Parking Violations</h1>
	</div>
	
<?php
require_once('query.php');
makeUserHeader();

$tableName = $_GET["table"];

// Write database information into file
$fileNameBase =  'dbBackups/' . $tableName . '_Backup_' . time();
$fileName = $fileNameBase . '.txt';
$file = fopen($fileName, "w");

// Write table name
fwrite($file, "Table:\n" );
fwrite($file, $tableName);

// Write column names
fwrite($file, "\nHeaders:\n");
$headers = getSQLHeaders( $tableName );
foreach( $headers as $header )
{
	fwrite($file, "$header\t"); 
}

// Write each record
fwrite($file, "\nRecords:\n");
$records = getSQLData( $tableName );
foreach( $records as $record )
{
	foreach( $record as $field )
	{
		fwrite($file, "$field\t"); 
	}
	fwrite($file, "\n");
}

fclose($file);

// Compress backup
$cfileName = $fileNameBase . ".gz";
$file = gzopen ($cfileName, 'w9');
gzwrite ($file, file_get_contents($fileName));
gzclose($file);

// Make file downloadable
chmod($cfileName, 644);

// Prompt user with download link
echo '<p><a href="' . $cfileName . '">Download ' . $tableName . ' Backup</a></p>';

?>