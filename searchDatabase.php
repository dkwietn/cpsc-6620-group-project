<?php
require_once('query.php');
// search table on specified column for specified value
echo '<table border="1px" align="center" class="sortable">';
$table = $_GET["table"];
// print table headers
$sql = "SHOW COLUMNS FROM " . $table ;
$result = querySQL($sql);
printSQLHeaders( $result );
$search = $_GET["search"];
$column = $_GET["column"];
//$whereClause = makeMultiColumnQuery( "citation", $search );
$whereClause = makeSingleColumnQuery($table, $column, $search);
// print records matching search
$sql = "SELECT * FROM " . $table . " WHERE " . $whereClause;
queryGetPage( $sql, $_GET["page"], $_GET["size"] );
echo '</table>';
?>