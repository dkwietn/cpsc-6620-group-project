<!DOCTYPE html>
<html>
	<head>
		<link type="text/css" rel="stylesheet" href="stylesheet.css"/>
		<title>Parking Violations</title>
	</head>
	<body>

<?php
require_once('query.php');

// log out user
removeCookie();
if(!isset($_SESSION)) {
	session_start();
}

session_unset();
session_destroy();

// redirect user to main view if logout fails for some reason
// or redirect user to login page on successful logout
if(isset($_COOKIE["User"]))
{
	echo "<div id='confirm'><p>Logout Failed</p></div>";
	echo "<div id='continue'><p><a href='mainView.html'>Click to continue</a></p></div>";

}
else
{
	echo "<div id='confirm'><p>Logout Successful</p></div>";
	echo "<div id='continue'><p><a href='index.html'>Click to continue</a></p></div>";
}

?>

</body>
</html>