<!DOCTYPE html>
<html>
	<head>
		<link type="text/css" rel="stylesheet" href="stylesheet.css"/>
		<title>Parking Violations</title>
	</head>
	<body>
	<div id="header">
		<h1 style="text-align:center">Manage User Accounts</h1>
	</div>
	
	<?php
		require_once('query.php');
		makeUserHeader();
	?>

	
	<div id="section">
		<p><input id="searchUsers" type="text"/> <button type="button" onclick="searchUsers(document.getElementById('resultsTable'), 1, 25, document.getElementById('searchUsers').value)">Search</button></p>
		<p><button type="button" onclick="showAllUsers(document.getElementById('resultsTable'), 1, 25)">Show All Users</button></p>
		
		<div id="resultsTable">

		</div>
	</div>
	
	<div id="footer">
	<p style="text-align:center"><?php getDisclaimer(); ?></p>
	</div>
		<script src="scripts.js"></script>
	</body>
</html>
