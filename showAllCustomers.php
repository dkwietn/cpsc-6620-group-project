<?php
require_once('query.php');
// show all records in customer table
echo '<table id="citTable" border="1px" align="center" class="sortable">';
// print headers
$sql = "SHOW COLUMNS FROM customer";
$result = querySQL($sql);
printSQLHeaders( $result );
// print paginated records
$sql = "SELECT * FROM customer";
queryGetPage( $sql, $_GET["page"], $_GET["size"] );
echo '</table>';
?>