<!DOCTYPE html>
<html>
	<head>
		<link type="text/css" rel="stylesheet" href="stylesheet.css"/>
		<title>About</title>
		<script src="scripts.js"></script>
	</head>
	<body>
	<h1>About</h1>
	<div id = "header"></div>

	<?php
		require_once('query.php');
		makeUserHeader();
	?>
	
	
	<p>This website was designed for the CPSC 4620/6620 Database Group Project by David, Trevor, and Xin.</p>
	<p>The database backend provides information about parking citations that have occurred at Clemson University between August 2014 and August 2015 that can be queried by the use of this website.</p>
	
	<h2>Here Are Some Interesting Statistics</h2>
		
	</p>
	<h3>Customer with most citations</h3>
	<?php
	echo '<table border="1px">';
	echo '<thead>';
	echo "<th>Customer ID</th>";
	echo "<th>Classification</th>";
	echo "<th>Citations</th>";
	echo '</thead>';
	$search = "select  c2 as custid, classification, citations from (select c2,ve as vehid, color, model,MaxCounted as citations, sum(price) as p from (select c2, color, model,Max(counted) as MaxCounted, vehid as ve from (select v.*, c.price, custid as c2, count(*) as counted from citation c inner join vehicle v where c.vehid = v.vehid group by c.vehid order by count(*) desc) as counts) AS maxprice inner join citation c2 on c2.vehid = ve) AS t2 inner join customer on c2 = customer.custid";
	echoQueryResults($search);
	echo '</table>';
	?>
	<p>
	
	<p>
	<h3>Customer who owes the most fines</h3>
	<?php
	echo '<table border="1px">';
	echo '<thead>';
	echo "<th>Customer ID</th>";
	echo "<th>Classification</th>";
	echo "<th>Amount ($)</th>";
	echo '</thead>';
	$search = "select c2 as custid, classification, maxprice as price from (select ve ,model, color, maxprice, custid as c2 from (select Max(sumPrice) as maxprice,vehid as ve from (select vehid,sumPrice from (select vehid, sum(price) as sumPrice from citation group by vehid) as t1) as t2) as t3 inner join vehicle on vehicle.vehid = ve) as t4 inner join customer where customer.custid = c2";
	echoQueryResults($search);
	echo '</table>';
	?>
	</p>
	
	<p>
	<h3>Vehicle that got the most citations</h3>
	<?php
	echo '<table border="1px">';
	echo '<thead>';
	echo "<th>Vehicle ID</th>";
	echo "<th>Color</th>";
	echo "<th>Model</th>";
	echo "<th>Citations</th>";
	echo "<th>Amount ($)</th>";
	echo '</thead>';
	$search = "select ve as vehid, color, model,MaxCounted as citations, sum(price) from (select color, model,Max(counted) as MaxCounted, vehid as ve from (select v.*, c.price, count(*) as counted from citation c inner join vehicle v where c.vehid = v.vehid group by c.vehid order by count(*) desc) as counts) AS maxprice inner join citation c2 on c2.vehid = ve;";
	echoQueryResults($search);
	echo '</table>';
	?>
	</p>
	
	<p>
	<h3>Color of cars that got the most citations</h3>
	<?php
	echo '<table border="1px">';
	echo '<thead>';
	echo "<th>Color</th>";
	echo "<th>Citations</th>";
	echo '</thead>';
	$search = "select v.color,count(*) counts from vehicle v inner join citation c on c.vehid = v.vehid group by v.color order by count(*) desc limit 5";
	echoQueryResults($search);
	echo '</table>';
	?>
	</p>
	
	<p>
	<h3>Color of cars that got the fewest citations</h3>
	<?php
	echo '<table border="1px">';
	echo '<thead>';
	echo "<th>Color</th>";
	echo "<th>Citations</th>";
	echo '</thead>';
	$search = "select v.color,count(*) counts from vehicle v inner join citation c on c.vehid = v.vehid group by v.color order by count(*) asc limit 10";
	echoQueryResults($search);
	echo '</table>';
	?>
	</p>
	
	<p>
	<h3>Classification with most tickets</h3>
	<?php
	echo '<table border="1px">';
	echo '<thead>';
	echo "<th>Classification</th>";
	echo "<th>Citations</th>";
	echo '</thead>';
	$search = "select classification, count(*) as citations from (select custid as c1 from citation inner join vehicle on vehicle.vehid = citation.vehid group by citation.vehid) t1 inner join customer on c1 = customer.custid group by classification order by count(*) desc limit 10";
	echoQueryResults($search);
	echo '</table>';
	?>
	</p>
	
	<p>
	<h3>Classification with fewest tickets</h3>
	<?php
	echo '<table border="1px">';
	echo '<thead>';
	echo "<th>Classification</th>";
	echo "<th>Citations</th>";
	echo '</thead>';
	$search = "select classification, count(*) as citations from (select custid as c1 from citation inner join vehicle on vehicle.vehid = citation.vehid group by citation.vehid) t1 inner join customer on c1 = customer.custid group by classification order by count(*) asc limit 10";
	echoQueryResults($search);
	echo '</table>';
	?>
	</p>
	
	<p>
	<h3>Locations with most citations </h3>
	<?php
	echo '<table border="1px">';
	echo '<thead>';
	echo "<th>Location</th>";
	echo "<th>Citations</th>";
	echo '</thead>';
	$search = "select location,count(*) from citation group by location order by count(*) desc limit 10";
	echoQueryResults($search);
	echo '</table>';
	?>
	</p>
	
	<p>
	<h3>Locations with fewest citations </h3>
	<?php
	echo '<table border="1px">';
	echo '<thead>';
	echo "<th>Location</th>";
	echo "<th>Citations</th>";
	echo '</thead>';
	$search = "select location,count(*) from citation group by location order by count(*) asc limit 10";
	echoQueryResults($search);
	echo '</table>';
	?>
	</p>
	
	<div id="footer">
	</br></br>
	<p style="text-align:center"><?php getDisclaimer(); ?></p>
	</div>
	
	</body>
</html>





