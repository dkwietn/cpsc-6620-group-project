<!DOCTYPE html>
<html>
	<head>
		<link type="text/css" rel="stylesheet" href="stylesheet.css"/>
		<title>Parking Violations</title>
	</head>
	<body>
	<div id="header">
		<h1 style="text-align:center">Welcome to our Database of Clemson University Parking Violations</h1>
	</div>
	
<?php
require_once('query.php');
makeUserHeader();

// open uploaded table backup file
$fileName = $_FILES['backupFile']['tmp_name'];
$file = gzopen($fileName, 'r');

// get table name
$line = fgets( $file );
assert( $line === "Table:\n");
$tableName = fgets( $file );

// get table column names
$line = fgets( $file );
assert( $line === "Headers:\n");
$line =  fgetcsv($file, 0, "\t" );
$columnNames = "";
foreach( $line as $header )
{
	$columnNames .= $header . ",";
}
$columnNames = substr($columnNames, 0, -2);

$line = fgets( $file );
assert( $line === "Records:\n");

// delete records currently in table
$sql = "DELETE FROM $tableName";
querySQL($sql);

// insert table records from file
while ($line =  fgetcsv($file, 0, "\t" ))
{
	$values = "";
	foreach( $line as $field )
	{
		
		$values .=  "'" . $field . "'" . ",";
	}
	$values = substr($values, 0, -4);
	$sql = "INSERT INTO $tableName($columnNames) VALUES($values)";
	$chk = querySQL($sql);
	if( !$chk )
	{
		// display error message for each record that fails to insert
		echo $sql . " Failed";
		echo '<br>';
	}
}

echo "<p>Finished Restoring $tableName</p>";

gzclose($file);

?>