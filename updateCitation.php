<!DOCTYPE html>
<html>
	<head>
		<link type="text/css" rel="stylesheet" href="stylesheet.css"/>
		<title>Parking Violations</title>
	</head>
<?php
require_once('query.php');

// edit citation data
$wasSuccessful = 0;
$citnum = $_GET["citnum"];
$newCit = $_POST["citnum"];

$conn = getSQLConnection();

$varString = "citnum = ?, description = ?, cdate = ?, ctime = ?, location = ?, price = ?, vehid = ?";
$sql = "UPDATE citation SET " . $varString ." WHERE citnum = ?";
$stmt = $conn->stmt_init();
if( $stmt->prepare($sql) )
{
	if( $stmt->bind_param('ssssssss', $_POST["citnum"], $_POST["description"], $_POST["cdate"], $_POST["ctime"], $_POST["location"], $_POST["price"], $_POST["vehid"], $citnum) )
	{
		if( $stmt->execute() )
		{
			$wasSuccessful = 1;
		}
	}
}

$stmt->close();
$conn->close();
// if successful redirect the user to view the updated citation
// if unsuccessful redirect the user back to edit the citation
if( $wasSuccessful )
{
	echo "<div id='confirm'><p>Citation Updated Successfully</p></div>";
	echo "<div id='continue'><a href='showCitation.php?citnum=" . $newCit . "'>Click Here</a></div";
}
else
{
	echo "<div id='confirm'><p>Citation Update Failed</p></div>";
	echo "<a href='editCitation.php?citnum=" . $citnum . "'>Update Citation</a></div>";
}

?>
</html>