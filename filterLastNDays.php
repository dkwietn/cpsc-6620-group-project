<?php
// get citation records from last N day database for filter button
// echo results as a table with pages
require_once('query.php');
echo '<table border="1px" align="center" class="sortable">';
$sql = "SHOW COLUMNS FROM citation";
$result = querySQL($sql);
printSQLHeaders( $result );
$search = "";
// check if there is a search string to be filtered
if( isset($_GET['column']) && isset($_GET['searchString']) )
{
	$search = " AND " . makeSingleColumnQuery( 'citation', $_GET['column'], $_GET['searchString']);
}
// account for the fact that the database is not up to date
$lastDate = subtractFromToday($_GET["days"]);
$sql = "SELECT * FROM citation WHERE cdate > " . $lastDate . $search . ' ORDER BY cdate DESC';
// get page of records meeting search and filter criteria
queryGetPage( $sql, $_GET["page"], $_GET["size"] );
echo '</table>';
?>