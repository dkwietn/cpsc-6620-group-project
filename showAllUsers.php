<?php
require_once('query.php');
// show all user records
echo '<table border="1px" align="center" class="sortable">';
// print headers
$sql = "SHOW COLUMNS FROM user";
$result = querySQL($sql);
printSQLHeaders( $result );
// print paginated records
$sql = "SELECT * FROM user ORDER BY username DESC";
queryGetPage( $sql, $_GET["page"], $_GET["size"] );
echo '</table>';
?>