<!DOCTYPE html>
<html>
	<head>
		<link type="text/css" rel="stylesheet" href="stylesheet.css"/>
		<title>Parking Violations</title>
	</head>
<?php
require_once('query.php');

// edit vehicle information
$wasSuccessful = 0;
$vehid = $_GET["vehid"];
$newVeh = $_POST["vehid"];

$conn = getSQLConnection();

$varString = "vehid = ?, model = ?, color = ?, custid = ?";
$sql = "UPDATE vehicle SET " . $varString ." WHERE vehid = ?";
$stmt = $conn->stmt_init();
if( $stmt->prepare($sql) )
{
	if( $stmt->bind_param('sssss', $_POST["vehid"], $_POST["model"], $_POST["color"], $_POST["custid"], $vehid) )
	{
		if( $stmt->execute() )
		{
			$wasSuccessful = 1;
		}
	}
}

$stmt->close();
$conn->close();

// if successful redirect the user to view the updated vehicle
// if unsuccessful redirect the user back to edit the vehicle
if( $wasSuccessful )
{
	echo "<div id='confirm'><p>Vehicle Updated Successfully</p></div>";
	echo "<div id='continue'><a href='showVehicle.php?vehid=" . $newVeh . "'>Click Here</a></div>";
}
else
{
	echo "<div id='confirm><p>Vehicle Update Failed</p></div>";
	echo "<div id='continue'><a href='editVehicle.php?vehid=" . $vehid . "'>Update Vehicle</a>>/div>";
}

?>
</html>