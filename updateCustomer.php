<!DOCTYPE html>
<html>
	<head>
		<link type="text/css" rel="stylesheet" href="stylesheet.css"/>
		<title>Parking Violations</title>
	</head>
<?php
require_once('query.php');

// edit customer data
$wasSuccessful = 0;
$custid= $_GET["custid"];
$newCust = $_POST["custid"];

$conn = getSQLConnection();

$varString = "custid = ?, classification = ?";
$sql = "UPDATE customer SET " . $varString ." WHERE custid = ?";
$stmt = $conn->stmt_init();
if( $stmt->prepare($sql) )
{
	if( $stmt->bind_param('sss', $_POST["custid"], $_POST["classification"], $custid) )
	{
		if( $stmt->execute() )
		{
			$wasSuccessful = 1;
		}
	}
}

// if successful redirect the user to view the updated customer
// if unsuccessful redirect the user back to edit the customer
if( $wasSuccessful )
{
	echo "<div id='confirm'><p>Customer Updated Successfully</p></div>";
	echo "<div id='continue'><a href='showCustomer.php?custid=" . $newCust . "'>Click Here</a></div>";
}
else
{
	echo "<div id='confirm'><p>Customer Update Failed</p></div>";
	echo "<div id='continue'><a href='editCustomer.php?custid=" . $custid . "'>Update Customer</a></div>";
}

?>
</html>