<?php
// count number of records in table that meet search criteria for search button
require_once('query.php');
$search = $_GET["search"]; // search term
$column = $_GET["column"]; // column to search
$table = $_GET["table"]; // table to search
$whereClause = makeSingleColumnQuery( $table, $column, $search );
$sql = "SELECT COUNT(*) FROM " . $table . " WHERE " . $whereClause;
$result = querySQL( $sql );
$row = $result->fetch_row();
// echo number of records
echo $row[0];
?>