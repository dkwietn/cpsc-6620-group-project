<!DOCTYPE html>
<html>
	<head>
		<link type="text/css" rel="stylesheet" href="stylesheet.css"/>
		<title>Parking Violations</title>
	</head>
<?php
require_once('query.php');
// delete citation from citation table
$citnum = $_GET["citnum"];
//echo $citnum;

deleteRecord( 'citation', 'citnum', $citnum );

// redirect user back to main page
echo "<div id='confirm'><p>Citation Deleted</p></div>";
echo "<div id='continue'><a href='mainView.php'>Click Here</a></div>";

?>
</html>