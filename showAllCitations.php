<?php
require_once('query.php');
// show all records in citations table
echo '<table id="citTable" border="1px" align="center" class="sortable">';
// print headers
$sql = "SHOW COLUMNS FROM citation";
$result = querySQL($sql);
printSQLHeaders( $result );
// print paginated records
$sql = "SELECT * FROM citation ORDER BY cdate DESC";
queryGetPage( $sql, $_GET["page"], $_GET["size"] );
echo '</table>';
?>