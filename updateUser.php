<!DOCTYPE html>
<html>
	<head>
		<link type="text/css" rel="stylesheet" href="stylesheet.css"/>
		<title>Parking Violations</title>
	</head>
<?php
require_once('query.php');
// edit user data

$wasSuccessful = 0;
if(!isset($_SESSION)) {
	session_start();
}

// check if admin is editing user data, if not edit currently logged in account
if(isset($_SESSION["UserLevel"]) && $_SESSION["UserLevel"] === "ADMIN" && isset($_GET["username"] ) )
{
	$username = $_GET["username"];
}
else
{
	$username = $_SESSION["User"];
}

// make sure that the username was not deleted then update the account
if( $_POST["username"] != "" )
{
	$conn = getSQLConnection();

	$varString = "username = ?, fname = ?, lname = ?, email = ?, address = ?, phoneNum = ?, age = ?";
	
	// check if password needs to be updated
	$updatePassword = false;
	$passwordRegex = "/[A-Z]/";
	if(  $_POST["password"] != "" && $_POST["password"] === $_POST["confirm_password"] && preg_match( $passwordRegex, $_POST["password"] ) )
	{
		$updatePassword = true;
		$varString .= ", password = ?";
	}
	
	// get the user id
	$userID = getSingleQueryColumn( 'user', "userid", 'username', $username ); 
	$sql = "UPDATE user SET " . $varString ." WHERE userid = " . $userID;
	$stmt = $conn->stmt_init();
	
	if( $stmt->prepare($sql) )
	{
		// query changes depending on whether password is being updated
		if( $updatePassword )
		{
			$stmt->bind_param('ssssssis', $_POST["username"], $_POST["first_name"], $_POST["last_name"],  $_POST["email_address"], $_POST["mailing_address"], $_POST["telephone_number"], $_POST["age"], hash('md5', $_POST["password"]));
		}
		else
		{
			$stmt->bind_param('ssssssi',$_POST["username"], $_POST["first_name"], $_POST["last_name"],  $_POST["email_address"], $_POST["mailing_address"], $_POST["telephone_number"], $_POST["age"]);
		}

		if( $stmt->execute() )
		{
			$wasSuccessful = 1;
		}
	}

	$stmt->close();
	$conn->close();
}

// if successful redirect the user to view the main page
// if unsuccessful redirect the user back to edit the user
if( $wasSuccessful )
{
	// if username was updated, update session accordingly
	if( getSingleQueryColumn( 'user', "userid", 'username', $_SESSION["User"] )  === getSingleQueryColumn( 'user', "userid", 'username', $_POST["username"] ) )
		$_SESSION["User"] = $_POST["username"];
	echo "<div id='confirm'><p>User Updated Successfully</p></div>";
	echo "<div id='continue'><a href='mainView.php'>Click Here</a></div>";
}
else
{
	echo "<div id='confirm'><p>User Update Failed</p></div>";
	echo "<div id='continue'><a href='editUser.php?username='" . $username . "'>Update User</a></div>";
}

?>
</html>
