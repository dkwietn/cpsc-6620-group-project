<!DOCTYPE html>
<html>
	<head>
		<link type="text/css" rel="stylesheet" href="stylesheet.css"/>
		<title>Parking Violations</title>
	</head>
<?php
require_once('query.php');

// function to create new user account in user table
function createAccount( $username, $first_name, $last_name, $age, $telephone_number, $email_address, $isAdmin, $mailing_address, $password )
{
	$conn = getSQLConnection();
	$sql = "INSERT INTO user(username, fname, lname, email, address, phoneNum, age, password) VALUES(?,?,?,?,?,?,?,?)";
	$stmt = $conn->stmt_init();
	$rValue = 0;
	$password = hash( 'md5', $password );
	if( $stmt->prepare($sql) )
	{
		$userID = 1;
		if( $stmt->bind_param('ssssssis', $username, $first_name, $last_name, $email_address,  $mailing_address, $telephone_number, $age, $password ) )
		{
			if( $stmt->execute() )
			{
				$rValue = 1;
			}
		}
	}
	
	$stmt->close();
	$conn->close();
	return $rValue;
}

// verify password fields match and contain a capital letter
$passwordRegex = "/[A-Z]/";
$wasSuccessful = 0;
if( $_POST["username"] != "" && $_POST["password"] === $_POST["confirm_password"] && preg_match( $passwordRegex, $_POST["password"] ) )
{
	$wasSuccessful = createAccount( $_POST["username"], $_POST["first_name"], $_POST["last_name"], $_POST["age"], $_POST["telephone_number"], $_POST["email_address"], 0, $_POST["mailing_address"], $_POST["password"] );
}


if( $wasSuccessful )
{
	// log user in to newly created account
	$userLevel = getUserLevel( $_POST["username"], $_POST["password"]);
	$username = getUserName($_POST["username"], $_POST["password"]);
	setUserCookie($_POST["username"], $userLevel);

	if(!isset($_SESSION)) {
		session_start();
	}
	
	$_SESSION["Username"] = $username;
	$_SESSION["UserLevel"]  = $userLevel;
	$_SESSION["User"]            = $username;
	
	// allo user to navigate to main page
	echo "<div id='confirm'><p>Account Creation Successful</p></div>";
	echo "<div id='continue'><a href='mainView.php'>Click Here</a></div>";
}
else 
{
	// return user to account creation page
	echo "<div id ='confirm'><p>Account Creation Failed</p></div>";
	echo "<div id ='continue'><a href='createAccount.php'>Create Account</a></div>";
}

?>
</html>