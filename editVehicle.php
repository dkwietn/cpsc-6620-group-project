<!DOCTYPE html>
<html>
	<head>
		<link type="text/css" rel="stylesheet" href="stylesheet.css"/>
		<title>Parking Violations</title>
	</head>
	<body>
	
	<?php require_once('query.php'); ?>
	
	<div id="header">
		<h1 style="text-align:left">Edit Vehicle</h1>
	</div>
	
	<?php
		require_once('query.php');
		makeUserHeader();
	?>
	
	<p>
	<?php 
	// variables used to populate vehicle information fields
	$tableName = 'vehicle';
	$columnID = 'vehid';
	$value = $_GET["vehid"];
	?>
	
	<div id="section">
	<form action="<?php echo 'updateVehicle.php?vehid=' . $value ?>"  method="post">
	<fieldset>
		vehid:&nbsp;&nbsp;<br/>
		<input id = "new" type="text" name="vehid" value="<?php echo getSingleQueryColumn( $tableName, 'vehid', $columnID, $value ); ?>"/>
		<br/>
		model:&nbsp;&nbsp;<br/>
		<input type="text" name="model" value='<?php echo getSingleQueryColumn( $tableName, "model", $columnID, $value ); ?>'/>
		<br/>
		color:&nbsp;&nbsp;<br/>
		<input type="text" name="color" value='<?php echo getSingleQueryColumn( $tableName, "color", $columnID, $value ); ?>'/>
		<br/>
		custid:&nbsp;&nbsp;<br/>
		<input type="text" name="custid" value='<?php echo getSingleQueryColumn( $tableName, "custid", $columnID, $value ); ?>'/>
		<br/>
		<p>
		<input type="submit" value="Update"/>
		&nbsp;&nbsp;
		<button type="button"><a href="<?php echo 'showVehicle.php?vehid=' . $value ?>">Cancel</a></button>
		</p>
	</fieldset>
	</form>
	
	</div>
	
	
	<div id="footer">
	<p style="text-align:center"><?php getDisclaimer(); ?></p>
	</div>
	
	</body>
</html>
